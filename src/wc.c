/* Terrible implementation of the wc core util */
/* for licensing information please see included LICENSE file */

/* The WC utility lists the number of lines, words, and bytes contained in each input file */
/* A 'word' is defined as a string of characters delimited by white space characters. */

/* TODOs for program to be considered "complete" */
/* DONE: Enable multiple arguments at the same time (i.g. -lw for line and word count) */
/* TODO: Enable accepting files from STDIN */
/* DONE: Fix formatting to match original program */
/* Flags that TCU accepts */
/* HOLD: --libxo: Generate output via libxo in a selection of different human and machine readable formats.    */
/*       I will need to actually figure out what this argument /does/ to try to figure out how to implement it */
/* DONE: -L Write the length of the line containing the most bytes (default) or characters (when -m is provided) to standard output. */
/*        When more than one file argument is specified, the longest input line of all files is reported as the value of the final "total". */
/* DONE: -c The number of bytes in each input file is written to the standard output. This will cancel out an any prior usage of the -m option. */
/* DONE: -l the number of lines in each input file is written to the standard output. */
/* HOLD: -m The number of characters in each input file is written to the standard output. If the current locale does not support */
/*       multi-byte characters, this is equivalent to the -c option. This will cancel out and prior usage of the -c option. */
/* DONE: -w The number of words in each input file is written to the standard output. */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* Functions */

int main(int argc, char *argv[]) {
  /* Prep stuff */
  FILE *fp;
  char ch;
  /* This value will represent the first file in argc */
  int firstFile = 0;
  /* Initilizing values for counters */
  int lValue = 0, wValue = 0, bValue = 0;
  /* Initializing values for totals */
  int lTotal = 0, wTotal = 0, bTotal = 0;
  /* This will be used to iterate through files */
  int i = 1;

  if (argc == 1) printf("Please enter filename\n");


  int countChars = strcmp(argv[i], "-c");
  if (countChars == 0) i++;
  int countLines = strcmp(argv[i], "-l");
  if (countLines == 0) i++;
  int countWords = strcmp(argv[i], "-w");
  if (countWords == 0) i++;

  /* This argument is handled differently than the others */
  int longestLine = strcmp(argv[i], "-L");
  if (longestLine == 0) {
    i++;
    int llLine = 0;
    for (i = i; i < argc; i++) {
      fp = fopen(argv[i], "r");
      int lLine = 0;
      bValue = 0;
      while((ch = fgetc(fp)) != EOF) {
        if (ch == '\n') {
          if (bValue > lLine) {
            lLine = bValue;
          }
          bValue = 0;
        } else {
          bValue++;
        }
      }
      fclose(fp);
      printf("%8d %s\n", lLine, argv[i]);
      if (lLine > llLine) llLine = lLine;
    }
    if (argc > 3) printf("%4d Total\n", llLine);
    return 0;
  }

  /* rough draft of basic idea */
  firstFile = i + 1;
  for (i = i; i < argc; i++) {
    lValue = 0;
    wValue = 0;
    bValue = 0;
    fp = fopen(argv[i], "r");
    while((ch = fgetc(fp)) != EOF) {
      if (ch == '\n') {
        lValue++;
        ch = fgetc(fp);
        if(ch == '\n') {
          lValue++;
          bValue++;
        } else {
          ungetc(ch, fp);
        }
      }
      if ((ch == ' ') || (ch == '\n')) {
        wValue++;
      }
      bValue++;
    }
    if ((countLines == 39) && (countWords == 39) && (countChars == 39)) {
      printf("%8d%8d%8d %s\n", lValue, wValue, bValue, argv[i]);
      lTotal = lTotal + lValue;
      wTotal = wTotal + wValue;
      bTotal = bTotal + bValue;
      fclose(fp);
    } else {
      if (countLines == 0) {
        printf("%8d", lValue);
        lTotal = lTotal + lValue;
      }
      if (countWords == 0) {
        printf("%8d", wValue);
        wTotal = wTotal + wValue;
      }
      if (countChars == 0) {
        printf("%8d", bValue);
        bTotal = bTotal + bValue;
      }
      printf(" %s\n", argv[i]);
      fclose(fp);
    }
  }

  if (firstFile != i) {
    if (countLines == 39) printf("%8d", lTotal);
    if (countLines == 39) printf("%8d", wTotal);
    if (countLines == 39) printf("%8d", bTotal);
    printf( " total\n");
  }

  return 0;
}
