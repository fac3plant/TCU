/* Terrible implmentation of the core utility 'Echo' */
/* For licnese information see the LICENSE file included in the repo */
#include<stdio.h>
#include<string.h>

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("\t\n");
        return 0;
    }
    int value = strcmp(argv[1], "-n");
    if (value == 0) {
        printf("%s", argv[2]);
    } else {
        printf("%s\n", argv[1]);
    }
    return 0;
}
