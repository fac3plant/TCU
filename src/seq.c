/*
 *  -f, --format:       Use a printf style format
 *  -s, --separator:    Use string to separate numbers
 *  -t, --terminiator:  Use string to terminate sequence of numbers
 *  -w, --fixed-width:  Equlize the widths of all numbers by padding with zeros as necessary.
 */
#include<stdio.h>
#include<stdlib.h>

int sequence(long f_num, int incr, int l_num)
{
    long i;
    if (f_num < l_num) {
        for (i = f_num; i <= l_num; i = (i + incr)) {
            printf("%ld\n", i);
        }
    } else if (l_num < f_num) {
        for (i = l_num; i <= f_num; i = (i + incr)) {
            printf("%ld\n", i);
        }
    }

    return 0;
}

int main(const int argc, const char *argv[])
{
    printf("Number of arguments: %d\n", (argc - 1));
    long nums[argc - 1];
    int i, j;

    //nums[0] = strtold(argv[j], NULL);

    for (i = 1; i < argc; i++) {
      j = 0;
      printf("Beginning loop\n");
      nums[j] = strtol(argv[i], NULL, 10);
      printf("argument %d: %ld\n", i, nums[j]);
      printf("End loop\n\n");
    }

    int format = 0, sep = 0, term = 0, fw = 0;

    for (i = 0; i < argc; i++) {
        printf("%d in array is: %ld\n", i, nums[i]);
    }

    sequence(nums[0], 1, nums[1]);

    /*
    if (argc == 3) {
        sequence(nums[0], nums[1], nums[2]);
    } else {
        sequence(nums[0], 1, nums[1]);
    }
    */

}
