/*
 * PROG: -b: Number the non-blank output lines, starting at 1
 * TODO: -e: Display non-printing characters (see the -v option), and display a dollar sign ('$') at the end of each line
 * TODO: -l: Set an exclusive advisory lock on the standard output file descriptor.
 * DONE: -n: Number the output lines, starting at 1
 * TODO: -s: Squeeze multiple adjacent empty lines, causing the output to be single spaced
 * TODO: -t: Display non-printing characters (see the -v option), and display tab characters as '^I'
 * TODO: -u: Disable output buffering
 * TODO: -v: Display non-printing characters so they are visible. Control characters print as '^X' for control-X; the delete character (octal 0177) prints as '^?'. Non-ASCII characters (with the high bit set) are printed as 'M-' (for meta) followed by the character for the low 7 bits.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc, char *argv[]) {

  int nFiles = argc - 1;
  int i;
  char ch;
  FILE *fp;

  /* Implements the '-b' flag */
  /* This flag should be re-done to recycle */
  /* code rather than re-doing it */
  int bNums = strcmp(argv[1], "-b");
  if (bNums == 0) {
    int line = 1;
    for (i = 2; i <= nFiles; i++) {
      fp = fopen(argv[i], "r");
      if (fp == NULL)
        {
          perror("error while opening file\n");
          exit(EXIT_FAILURE);
        }
      printf("\t%d ", line);
      while((ch = fgetc(fp)) != EOF) {
        if (ch == '\n') {
          line++;
          printf("\n\t%d ", line);
          //printf("\n");
          ch = fgetc(fp);
          if (ch == '\n') {
            printf("\n");
          } else {
            ungetc(ch, fp);
          }
        } else {
          printf("%c", ch);
        }
      }
      fclose(fp);
    }
    return 0;
  }

  /* Implements the '-n' flag */
  int lNums = strcmp(argv[1], "-n");
  if (lNums == 0) {
    int line = 1;
    for (i = 2; i <= nFiles; i++) {
      fp = fopen(argv[i], "r");
      if (fp == NULL)
        {
          perror("error while opening file\n");
          exit(EXIT_FAILURE);
        }
      printf("\t%d  ", line);
      while((ch = fgetc(fp)) != EOF) {
        if (ch == '\n') {
          line++;
          printf("\n\t%d  ", line);
        } else {
          printf("%c", ch);
        }
      }

      fclose(fp);
  }
  return 0;
  }

  /* The following code creates the basic functionality of cat */
  for (i = 1; i <= nFiles; i++) {
    fp = fopen(argv[i], "r");
    if (fp == NULL)
      {
        perror("error while opening file\n");
        exit(EXIT_FAILURE);
      }
    while((ch = fgetc(fp)) != EOF) {
      printf("%c", ch);
    }

  fclose(fp);
  }
  return 0;
}
