CC = clang
all:
	for files in echo false head true yes ; do \
		$(CC) src/$${files}.c -o bin/$${files} ; \
	done

clean:
	rm bin/*

cat:
	$(CC) src/cat.c -o bin/cat
echo:
	$(CC) src/echo.c -o bin/echo
false:
	$(CC) src/false.c -o bin/false
head:
	$(CC) src/head.c -o bin/head
seq:
	$(CC) src/seq.c -o bin/seq
true:
	$(CC) src/true.c -o bin/true
wordcount:
	$(CC) src/wc.c -o bin/wc
yes:
	$(CC) src/yes.c -o bin/yes

install:
	echo "Please don't, just really don't"
